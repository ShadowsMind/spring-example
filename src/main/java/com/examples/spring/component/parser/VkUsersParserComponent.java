package com.examples.spring.component.parser;

import com.examples.spring.domain.dto.VkUserDto;

import java.util.List;

public interface VkUsersParserComponent {

    List<VkUserDto> findByIds(String vkIds);

    VkUserDto findById(String vkId);

}