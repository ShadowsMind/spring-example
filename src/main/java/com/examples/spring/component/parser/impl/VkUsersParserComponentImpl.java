package com.examples.spring.component.parser.impl;

import com.examples.spring.component.parser.VkUsersParserComponent;
import com.examples.spring.domain.dto.VkUserDto;
import com.examples.spring.domain.dto.VkUsersResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.social.support.URIBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Component
public class VkUsersParserComponentImpl implements VkUsersParserComponent {

    @Autowired
    RestTemplate restTemplate;

    final String vkApiUrl = "https://api.vk.com/method/users.get";
    final String apiVersion = "5.37";
    final String httpsEnabled = "1";


    @Override
    public List<VkUserDto> findByIds(String vkIds) {
        URI uri = buildUrl(vkIds);
        ResponseEntity<VkUsersResponseDto> vkResponse = restTemplate.getForEntity(uri, VkUsersResponseDto.class);
        VkUsersResponseDto vkUsersResponseDto = vkResponse.getBody();

        return vkUsersResponseDto.getVkUsers();
    }

    @Override
    public VkUserDto findById(String vkId) {
        return findByIds(vkId).get(0);
    }


    private URI buildUrl(String vkIds) {
        URIBuilder uriBuilder = URIBuilder.fromUri(vkApiUrl);
        uriBuilder.queryParam("user_ids", vkIds);
        uriBuilder.queryParam("v", apiVersion);
        uriBuilder.queryParam("https", httpsEnabled);

        return uriBuilder.build();
    }

}
