package com.examples.spring.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = { "com.examples.spring.repository" })
@EnableTransactionManagement
@PropertySource(value = "classpath:/database.properties")
public class DatasourceConfig {

    String HIBERNATE_DIALECT            = "hibernate.dialect";
    String HIBERNATE_AUTO               = "hibernate.hbm2ddl.auto";
    String ENTITIES_PATH                = "com.examples.spring.domain.entity";
    String HIBERNATE_SCHEMA             = "hibernate.default_schema";
    String HIBERNATE_SHOW_SQL           = "hibernate.show_sql";
    String HIBERNATE_FORMAT_SQL         = "hibernate.format_sql";
    String HIBERNATE_SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";


    @Autowired
    Environment environment;


    @Bean(name = "dataSource")
    DriverManagerDataSource dataSource() {
        DriverManagerDataSource datasource = new DriverManagerDataSource();
        datasource.setDriverClassName(environment.getProperty("db.driver"));
        datasource.setUrl(environment.getProperty("db.url"));
        datasource.setUsername(environment.getProperty("db.user"));
        datasource.setPassword(environment.getProperty("db.password"));

        return datasource;
    }

    @Bean(name = "entityManagerFactory")
    LocalContainerEntityManagerFactoryBean entityManager() {
        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
        entityManager.setDataSource(dataSource());
        entityManager.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManager.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManager.setPackagesToScan(ENTITIES_PATH);
        entityManager.setJpaProperties(jpaProperties());

        return entityManager;
    }

    @Bean(name = "transactionManager")
    JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManager().getObject());

        return transactionManager;
    }


    private Properties jpaProperties() {
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty(HIBERNATE_DIALECT, environment.getProperty("db.dialect"));
        //jpaProperties.setProperty(HIBERNATE_SCHEMA, environment.getProperty("db.schema"));
        jpaProperties.setProperty(HIBERNATE_SHOW_SQL, environment.getProperty("db.show_sql"));
        jpaProperties.setProperty(HIBERNATE_FORMAT_SQL, environment.getProperty("db.format_sql"));
        jpaProperties.setProperty(HIBERNATE_SECOND_LEVEL_CACHE, environment.getProperty("db.hibernate_use_second_level_cache"));
        jpaProperties.setProperty(HIBERNATE_AUTO, environment.getProperty("db.hbm2ddl"));


        return jpaProperties;
    }

}
