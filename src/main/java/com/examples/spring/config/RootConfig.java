package com.examples.spring.config;

import com.examples.spring.domain.SiteInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackages = "com.examples.spring")
@PropertySource(value = {"classpath:/app.properties"})
public class RootConfig {

    @Autowired
    Environment environment;


    @Bean(name = "siteInformation")
    SiteInformation siteInformation() {
        String name = environment.getProperty("app.name");
        String url = environment.getProperty("app.url");
        String date = environment.getProperty("app.creation_date");

        return new SiteInformation(name, url, date);
    }


    @Bean(name = "restTemplate")
    RestTemplate restTemplate() {
        return new RestTemplate();
    }


}
