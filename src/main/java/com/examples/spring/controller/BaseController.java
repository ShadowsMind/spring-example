package com.examples.spring.controller;

import com.examples.spring.domain.SiteInformation;
import com.examples.spring.util.NumberProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@RequestMapping
public class BaseController {

    @Autowired
    SiteInformation siteInformation;

    @Autowired
    HttpServletRequest request;


    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public SiteInformation sayHello() {
        return siteInformation;
    }

    @RequestMapping(value = "params", method = RequestMethod.GET, produces = "application/json")
    public String getParams(HttpServletResponse response) {
        response.addCookie(new Cookie("test", UUID.randomUUID().toString()));
        response.setStatus(404);

        return request.getRemoteAddr();
    }

    @RequestMapping(value = "calculator", method = RequestMethod.POST)
    public Integer getSum(@RequestParam Integer first, @RequestParam Integer second) {
        return NumberProcessor.sum(first, second);
    }


}
