package com.examples.spring.controller;


import com.examples.spring.domain.SiteInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping
public class BasePagesController {

    @Autowired
    SiteInformation siteInformation;



    @RequestMapping(value = "home", method = RequestMethod.GET)
    public ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("siteInformation", siteInformation);

        return modelAndView;
    }



}
