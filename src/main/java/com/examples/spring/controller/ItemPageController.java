package com.examples.spring.controller;

import com.examples.spring.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping
public class ItemPageController {


    @Autowired
    ItemRepository itemRepository;


    @RequestMapping(value = "items", method = RequestMethod.GET)
    public ModelAndView itemPage() {
        ModelAndView modelAndView = new ModelAndView("item");

        modelAndView.addObject("items", itemRepository.findAll());
        modelAndView.addObject("title", "Items page");
        modelAndView.addObject("isNew", false);

        return modelAndView;
    }




}
