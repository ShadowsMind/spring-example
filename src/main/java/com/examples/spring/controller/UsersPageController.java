package com.examples.spring.controller;

import com.examples.spring.domain.User;
import com.examples.spring.service.UsersHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

@Controller
@RequestMapping
public class UsersPageController {



    @RequestMapping(value = "users", method = RequestMethod.GET)
    public ModelAndView users() {
        ModelAndView modelAndView = new ModelAndView("users");

        Collection<User> users = UsersHolder.getUsers().values();

        modelAndView.addObject("users", users);

        return modelAndView;
    }

}
