package com.examples.spring.controller.rest;

import com.examples.spring.domain.entity.Account;
import com.examples.spring.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/", produces = "application/json")
public class AccountRestController {

    @Autowired
    AccountRepository accountRepository;


    @RequestMapping(value = "accounts", method = RequestMethod.GET)
    public List<Account> findAll() {
        return (List<Account>) accountRepository.findAll();
    }

    @RequestMapping(value = "accounts", method = RequestMethod.POST)
    public Account create(@RequestParam String email,
                          @RequestParam String firstName,
                          @RequestParam String lastName,
                          @RequestParam String password) {
        Account account = new Account(null, email, firstName, lastName, password);
        return accountRepository.save(account);
    }

    @RequestMapping(value = "accounts/current", method = RequestMethod.GET)
    public Object findCurrent() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
