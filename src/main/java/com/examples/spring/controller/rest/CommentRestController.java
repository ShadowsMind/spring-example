package com.examples.spring.controller.rest;

import com.examples.spring.domain.entity.Comment;
import com.examples.spring.service.CommentService;
import com.examples.spring.validation.CommentTextValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "api/")
public class CommentRestController {

    @Autowired
    CommentService commentService;

    @Autowired
    CommentTextValidator commentTextValidator;


    @RequestMapping(value = "comments", method = RequestMethod.POST)
    public Map<String, Object> create(@RequestParam Long authorId, @RequestParam String text) {
        Map<String, Object> result = new HashMap<>();

        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(text, "email");
        commentTextValidator.validate(text, errors);

        if (errors.hasErrors()) {
            result.put("status", "validationError");
        } else {
            Comment comment = commentService.create(authorId, text);

            result.put("comment", comment);
            result.put("status", "success");
        }

        return result;
    }


}
