package com.examples.spring.controller.rest;

import com.examples.spring.domain.dto.ItemDto;
import com.examples.spring.domain.entity.Item;
import com.examples.spring.service.ItemService;
import com.examples.spring.util.ResponseHelper;
import com.examples.spring.validation.ItemValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "api/", produces = "application/json")
public class ItemRestController {

    @Autowired
    private ItemValidator itemValidator;

    @Autowired
    private ItemService itemService;


    @InitBinder(value = "itemDto")
    public void initItemBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(itemValidator);
    }


    @RequestMapping(value = "items", method = RequestMethod.POST)
    public Map<String, Object> createItem(@ModelAttribute @Valid ItemDto itemDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseHelper.validationError(bindingResult);
        } else {
            Item item = itemService.create(itemDto);
            return ResponseHelper.success("item", item);
        }
    }

}
