package com.examples.spring.controller.rest;

import com.examples.spring.domain.User;
import com.examples.spring.service.UsersHolder;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "api/")
public class UserRestController {

    @Autowired
    HttpServletRequest request;

    @Autowired
    Validator validator;


    @InitBinder(value = "user")
    public void initUserValidator(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(validator);
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = NotFoundException.class)
    public String notFoundHandler(NotFoundException ex) {
        return ex.getMessage();
    }


    @RequestMapping(value = "users/{id}", method = RequestMethod.GET, produces = "application/json")
    public User findUser(@PathVariable("id") String id) throws NotFoundException {
        if (UsersHolder.getUsers().containsKey(id)) {
            return UsersHolder.getUsers().get(id);
        } else {
            throw new NotFoundException("user with id" + id + "not found");
        }
    }

    @RequestMapping(value = "users", method = RequestMethod.GET, produces = "application/json")
    public Map<String, User> findAllUsers() {
        return UsersHolder.getUsers();
    }

    @RequestMapping(value = "users", method = RequestMethod.POST, produces = "application/json")
    public String saveUser(@ModelAttribute @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "error";
        } else {
            String id = UUID.randomUUID().toString();
            UsersHolder.addUser(id, user);

            return "success";
        }
    }

    @RequestMapping(value = "users/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteUser(@PathVariable("id") String id) throws NotFoundException {
        if (UsersHolder.getUsers().containsKey(id)) {
            UsersHolder.deleteUser(id);

            return "success";
        } else {
            throw new NotFoundException("user can't be deleted cause not exists");
        }
    }

}
