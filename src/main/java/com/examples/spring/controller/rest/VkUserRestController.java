package com.examples.spring.controller.rest;

import com.examples.spring.component.parser.VkUsersParserComponent;
import com.examples.spring.domain.dto.VkUserDto;
import com.examples.spring.util.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/vk/users")
public class VkUserRestController {

    @Autowired
    VkUsersParserComponent vkUsersParserComponent;


    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> findAll(@RequestParam String vkIds) {
        List<VkUserDto> vkUsers = vkUsersParserComponent.findByIds(vkIds);

        return ResponseHelper.success("vkUsers", vkUsers);
    }

}
