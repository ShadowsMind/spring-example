package com.examples.spring.domain;

import java.io.Serializable;

public class SiteInformation implements Serializable {

    public SiteInformation() {
    }

    public SiteInformation(String name, String url, String creationDate) {
        this.name = name;
        this.url = url;
        this.creationDate = creationDate;
    }

    private String name;
    private String url;
    private String creationDate;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}
