package com.examples.spring.domain.dto;

import java.io.Serializable;

public class ItemDto implements Serializable {


    public ItemDto() {
    }

    public ItemDto(String title, String text, String category, String author) {
        this.title = title;
        this.text = text;
        this.category = category;
        this.author = author;
    }

    private String title;
    private String text;
    private String category;
    private String author;


    @Override
    public String toString() {
        return "ItemDto{" +
                "title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", category='" + category + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
