package com.examples.spring.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class VkUsersResponseDto implements Serializable {

    public VkUsersResponseDto() {
    }

    public VkUsersResponseDto(List<VkUserDto> vkUsers) {
        this.vkUsers = vkUsers;
    }

    @JsonProperty("response")
    List<VkUserDto> vkUsers;


    public List<VkUserDto> getVkUsers() {
        return vkUsers;
    }

    public void setVkUsers(List<VkUserDto> vkUsers) {
        this.vkUsers = vkUsers;
    }
}
