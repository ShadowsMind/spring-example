package com.examples.spring.domain.dto.converter;

import com.examples.spring.domain.dto.ItemDto;
import com.examples.spring.domain.entity.Item;

import java.util.Date;

public class ItemConverter {

    public static Item convert(ItemDto itemDto) {
        return new Item(
                itemDto.getTitle(),
                itemDto.getText(),
                itemDto.getCategory(),
                itemDto.getAuthor(),
                new Date()
        );
    }

}
