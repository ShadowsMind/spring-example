package com.examples.spring.domain.entity;

import com.examples.spring.domain.entity.column.SequenceId;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address extends SequenceId {

    public Address() {
    }

    public Address(Account account, String street, String city, String county) {
        this.street = street;
        this.city = city;
        this.county = county;
        this.account = account;
    }


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "street")
    private String street;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String county;

    @Override
    public String toString() {
        return "Address{" +
                "id=" + getId() +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                '}';
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

}
