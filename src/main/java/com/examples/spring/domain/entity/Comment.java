package com.examples.spring.domain.entity;

import com.examples.spring.domain.entity.column.SequenceId;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comment")
public class Comment extends SequenceId {

    public Comment() {
    }

    public Comment(Account author, String text, Date date) {
        this.author = author;
        this.text = text;
        this.createdAt = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id")
    private Account author;


    @Column(name = "text")
    private String text;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Comment parent;


    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Override
    public String toString() {
        return "Comment{" +
                "author=" + author +
                ", text='" + text + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
