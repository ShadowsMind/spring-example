package com.examples.spring.repository;

import com.examples.spring.domain.entity.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Query("select a from Account a where a.email=?1")
    Account findOneByEmail(String email);

}
