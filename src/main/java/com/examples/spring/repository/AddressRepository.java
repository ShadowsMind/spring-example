package com.examples.spring.repository;

import com.examples.spring.domain.entity.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
}
