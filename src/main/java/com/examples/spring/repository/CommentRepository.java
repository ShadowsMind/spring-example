package com.examples.spring.repository;

import com.examples.spring.domain.entity.Comment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {

    @Modifying
    @Query("update Comment c set c.text=?2 where c.id=?1")
    public void updateText(Long id, String text);

}
