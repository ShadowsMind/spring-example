package com.examples.spring.repository;

import com.examples.spring.domain.entity.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
