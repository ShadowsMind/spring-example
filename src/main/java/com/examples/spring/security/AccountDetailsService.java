package com.examples.spring.security;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface AccountDetailsService extends UserDetailsService {
}
