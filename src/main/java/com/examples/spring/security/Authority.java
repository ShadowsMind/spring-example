package com.examples.spring.security;

import org.springframework.security.core.GrantedAuthority;

public class Authority implements GrantedAuthority {

    public Authority() {
    }

    public Authority(String authority) {
        this.authority = authority;
    }

    private String authority;

    @Override
    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
