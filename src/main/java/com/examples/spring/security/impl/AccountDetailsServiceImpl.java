package com.examples.spring.security.impl;

import com.examples.spring.domain.entity.Account;
import com.examples.spring.repository.AccountRepository;
import com.examples.spring.security.AccountDetails;
import com.examples.spring.security.AccountDetailsService;
import com.examples.spring.security.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AccountDetailsServiceImpl implements AccountDetailsService {

    @Autowired
    AccountRepository accountRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findOneByEmail(email);
        if (account != null) {
            Set<Authority> authorities = new HashSet<>();
            authorities.add(new Authority("USER"));
            AccountDetails accountDetails = new AccountDetails(account.getFirstName(), account.getLastName(), account.getEmail(), account.getPassword(), true, authorities);

            return accountDetails;
        } else {
            return null;
        }
    }

}
