package com.examples.spring.service;

import com.examples.spring.domain.entity.Comment;

public interface CommentService {

    Comment create(Long authorId, String text);

}