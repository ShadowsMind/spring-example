package com.examples.spring.service;

import com.examples.spring.domain.dto.ItemDto;
import com.examples.spring.domain.entity.Item;

public interface ItemService {

    Item create(ItemDto itemDto);

}
