package com.examples.spring.service;

import com.examples.spring.domain.User;

import java.util.HashMap;
import java.util.Map;

public class UsersHolder {

    private static Map<String, User> users = new HashMap<>();


    public synchronized static Map<String, User> getUsers() {
        return users;
    }

    public synchronized static void setUsers(Map<String, User> users) {
        UsersHolder.users = users;
    }

    public synchronized static void addUser(String id, User user) {
        users.put(id, user);
    }

    public synchronized static void deleteUser(String id) {
        users.remove(id);
    }

}
