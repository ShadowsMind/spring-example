package com.examples.spring.service.transactional;

import com.examples.spring.domain.entity.Account;
import com.examples.spring.domain.entity.Comment;
import com.examples.spring.repository.AccountRepository;
import com.examples.spring.repository.CommentRepository;
import com.examples.spring.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class CommentServiceTransactional implements CommentService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CommentRepository commentRepository;


    @Transactional
    @Override
    public Comment create(Long authorId, String text) {
        Account author = accountRepository.findOne(authorId);
        if (authorId == null) throw new IllegalArgumentException();
        if (author == null) throw new NullPointerException();
        Comment comment = new Comment(author, text, new Date());

        Comment result = commentRepository.save(comment);

        if (authorId == 2) throw new NullPointerException();

        return result;
    }

}
