package com.examples.spring.service.transactional;

import com.examples.spring.domain.dto.ItemDto;
import com.examples.spring.domain.dto.converter.ItemConverter;
import com.examples.spring.domain.entity.Item;
import com.examples.spring.repository.ItemRepository;
import com.examples.spring.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ItemServiceTransactional implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Transactional
    @Override
    public Item create(ItemDto itemDto) {
        Item item = ItemConverter.convert(itemDto);

        return itemRepository.save(item);
    }

}
