package com.examples.spring.util;

import org.springframework.validation.BindingResult;

import java.util.HashMap;
import java.util.Map;

public class ResponseHelper {

    public static Map<String, Object> success(String objectName, Object obj) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        response.put(objectName, obj);
        result.put("status", "success");
        result.put("response", response);

        return result;
    }

    public static Map<String, Object> validationError(BindingResult bindingResult) {
        Map<String, Object> result = new HashMap<>();

        result.put("status", "validationError");
        result.put("errors", bindingResult.getAllErrors());

        return result;
    }

}
