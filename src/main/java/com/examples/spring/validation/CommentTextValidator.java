package com.examples.spring.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CommentTextValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(String.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        String text = (String) target;

        if (text == null) {
            errors.reject("Null", "Text can't be null");
            return;
        }

        if (text.isEmpty()) {
            errors.reject("Empty", "Text can't be empty");
            return;
        }

        if (text.contains("!!!!!")) {
            errors.reject("HateThis", "Text can't contains !!!!!");
            return;
        }

    }

}
