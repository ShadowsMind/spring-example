package com.examples.spring.validation;

import com.examples.spring.domain.dto.ItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ItemValidator implements Validator {

    @Autowired
    Validator validator;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(ItemDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ItemDto item = (ItemDto) target;

        validator.validate(item, errors);

        if (item.getText().length() > 2048) {
            errors.rejectValue("text", "WrongLength", "Длинна текста статьи должна быть меньше 2048 симоволов");
        }
        if (item.getTitle().length() < 10) {
            errors.rejectValue("text", "WrongLength", "Длинна текста статьи должна быть больше 10 симоволов");
        }
    }

}
