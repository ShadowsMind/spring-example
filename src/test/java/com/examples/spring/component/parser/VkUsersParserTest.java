package com.examples.spring.component.parser;

import com.examples.spring.config.RootConfig;
import com.examples.spring.domain.dto.VkUserDto;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RootConfig.class})
@WebAppConfiguration
public class VkUsersParserTest {

    Logger logger = Logger.getLogger(VkUsersParserTest.class);

    @Autowired
    VkUsersParserComponent vkUsersParserComponent;


    @Test
    public void testVkUsersFind() {
        VkUserDto vkUser = vkUsersParserComponent.findById("shadowsmind");

        logger.info("result: " + vkUser.toString());
    }

    @Test
    public void testFindAll() {
        List<VkUserDto> vkUsers = vkUsersParserComponent.findByIds("shadowsmind,id62627663");

        vkUsers.forEach(user -> logger.info("result: " + user.toString()));
    }


}
