package com.examples.spring.config;

import com.examples.spring.domain.dto.VkUsersResponseDto;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RootConfig.class})
@WebAppConfiguration
public class RestTemplateTest {

    Logger logger = Logger.getLogger(RestTemplateTest.class);

    @Autowired
    RestTemplate restTemplate;


    @Test
    public void testGet() {
        ResponseEntity<String> stringResponse = restTemplate.getForEntity("https://google.com", String.class);

        logger.info(stringResponse.getStatusCode());
        logger.info(stringResponse.getBody());
    }

    @Test
    public void testDtoMapping() {
        ResponseEntity<VkUsersResponseDto> vkResponse = restTemplate.getForEntity("https://api.vk.com/method/users.get?user_ids=shadowsmind", VkUsersResponseDto.class);

        logger.info("vk responsed with status: " + vkResponse.getStatusCode());
    }


}
