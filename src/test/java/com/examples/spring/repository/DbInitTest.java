package com.examples.spring.repository;

import com.examples.spring.config.DatasourceConfig;
import com.examples.spring.domain.entity.Account;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatasourceConfig.class})
@Transactional
public class DbInitTest {

    Logger logger = Logger.getLogger(DbInitTest.class);

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    CommentRepository commentRepository;


    @Test
    @Rollback(value = false)
    public void init() {
        Account account = new Account(null, "test@mail.ru", "Vasya", "Petrov", "1234");
        accountRepository.save(account);
    }


}
